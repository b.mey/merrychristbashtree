## USAGE
```shell
curl https://gitlab.com/b.mey/merrychristbashtree/-/raw/main/tree.sh | bash
```

or

```shell
wget -qO- https://gitlab.com/b.mey/merrychristbashtree/-/raw/main/tree.sh | bash
```
